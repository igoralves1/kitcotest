kitcotest


Service Development Test

 

The purpose of this test is to evaluate your proficiency in the following areas.

1-                  Technical aspects of Node.js development

2-                  Technical aspect of service development

3-                  Coding standard, Best practices and Unit Test

4-                  Familiarity with cloud based development, specifically in the context of AWS

5-                  Understanding the problem statement

6-                  Time estimate

7-                  Deliverables

You are required to develop and deploy the following two services. Considering the time limit, you have to have a strategy for using your time. You are not required to finish all the development perfectly but for any unfinished area, it is important to provide a solution summary/plan.

 

1- Design and develop a Lambda function (with integrated Unit Tests) that once triggered reads the following feed and writes a json file version of the information (converted from XML) into a bucket (kitcotestbucket) in S3.

The unit test could be simply checking if the bid value is smaller than the ask value and they are both numbers.

https://kds2.kitco.com/getQuote?symbol=AU,AG&apikey=9bnteWVi2kT13528d100c608fn0TlbC6&type=xml

Here is a sample output of the feed:

<Quotes><PM><Symbol>AU</Symbol><Timestamp>2018-11-19 18:19:02</Timestamp><Bid>1223.50</Bid><Ask>1224.50</Ask><Change>-0.40</Change><ChangePercentage>-0.03</ChangePercentage><Low>1223.60</Low><High>1225.00</High></PM><PM><Symbol>AG</Symbol><Timestamp>2018-11-19 18:14:49</Timestamp><Bid>14.40</Bid><Ask>14.51</Ask><Change>0.00</Change><ChangePercentage>0.00</ChangePercentage><Low>14.30</Low><High>14.51</High></PM></Quotes>

2-Develop another Lambda function that runs every time there is new Json files in the bucket and writes in logs the number of json files in the bucket. 