'use strict';

const request = require('request');
const xmlParser = require('xml2json');

module.exports.hello = async(event, context) => {
    // Retrieve XML form the URL
    return new Promise((resolved, reject) => {
        request('https://kds2.kitco.com/getQuote?symbol=AU,AG&apikey=9bnteWVi2kT13528d100c608fn0TlbC6&type=xml', function(error, response, body) {
            // Convert XML into JSON
            if (!error && response.statusCode == 200) {
                const jsonResp = xmlParser.toJson(body);
                // console.log('JSON output', jsonResp);
                resolved({
                    statusCode: response.statusCode,
                    body: jsonResp,
                });
            } else {
                console.log("Error " + response.statusCode);
                reject(response.statusCode);
            }
        })
    });
};